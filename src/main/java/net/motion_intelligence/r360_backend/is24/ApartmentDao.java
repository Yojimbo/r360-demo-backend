package net.motion_intelligence.r360_backend.is24;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.motion_intelligence.r360_backend.R360BackendConfig;
import net.motion_intelligence.r360_backend.hibernate.HibernateUtil;
import net.motion_intelligence.r360_backend.pojo.Apartment;

public class ApartmentDao {
	
	/**
	 * 
	 */
	private static R360BackendConfig CONFIG = R360BackendConfig.loadConfig();
	
	/**
	 * 
	 */
	private Connection backendConnection;
	
	/**
	 * 
	 */
	public ApartmentDao() {
		
		connectDB();
	}
	
	private static final Logger LOGGER 			= LoggerFactory.getLogger(Immobilienscout24ImportTask.class);
	
	
//	public List<Apartment> getAllApartments() {
//		
//		Session session = HibernateUtil.getInstance().getSessionFactory().openSession();
//		
//		Criteria query = session.createCriteria(Apartment.class)
//			.add(Restrictions.ge("lat", bottomRightLat)).add(Restrictions.le("lat", topLeftLat))
//			.add(Restrictions.ge("lon", topLeftLon)).add(Restrictions.le("lon", bottomRightLon))
//			.createAlias("tags", "tag").add(Restrictions.in("tag.value", tags))
//			.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
//		
//		List<PointOfInterest> list = query.list();
//		session.close();
//		
//		return list;
//	}
	
	public List<Apartment> getApartments(double minPrice, double maxPrice, double minAra, double maxArea, double minRooms, double maxRooms, boolean kitchen, boolean garden, boolean courtageFree, boolean balcony) {
		
		Session session = HibernateUtil.getInstance().getSessionFactory().openSession();
		
		Criteria query = session.createCriteria(Apartment.class)
			.add(Restrictions.ge("price", minPrice)).add(Restrictions.le("price", maxPrice))
			.add(Restrictions.ge("area", minAra)).add(Restrictions.le("area", maxArea))
			.add(Restrictions.ge("rooms", minRooms)).add(Restrictions.le("rooms", maxRooms));
		
		if ( balcony ) 		query.add(Restrictions.eq("balcony", balcony));
		if ( courtageFree ) query.add(Restrictions.eq("courtage", !courtageFree));
		if ( garden ) 		query.add(Restrictions.eq("garden", garden));
		if ( kitchen ) 		query.add(Restrictions.eq("kitchen", kitchen));
		
		List<Apartment> list = query.list();
		session.close();
		
		return list;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Apartment> getAllApartments() {
		
		Session session = HibernateUtil.getInstance().getSessionFactory().openSession();
		
		@SuppressWarnings("unchecked")
		List<Apartment> apartments = session.createCriteria(Apartment.class).list();
		session.close();
		
//		return apartments.subList(0, Math.min(apartments.size(), 100));
		return apartments;
	}
	
	/**
	 * @return
	 */
	private void connectDB() {

		try {
			
			Class.forName("org.postgresql.Driver");
			backendConnection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/" + 
					CONFIG.getStringSetting("database", "name"), 
					CONFIG.getStringSetting("database", "user"), 
					CONFIG.getStringSetting("database", "password"));
		}
		catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
		}
	}
	
	/**
	 * @param apiConnection
	 * @throws SQLException
	 */
	public void createNewTable() {

		try {
			
			Statement statement = backendConnection.createStatement();
			statement.execute(StringUtils.join(new String[] {
					"CREATE TABLE IF NOT EXISTS apartment ( ", 
					"   id integer  NOT NULL,"  , 
					"   title varchar, ", 
					"   modification varchar,"  , 
					"   creation varchar, ", 
					"   publishDate varchar,"  , 
					"   realEstateId integer NOT NULL, ", 
					"   street varchar,"  , 
					"   houseNumber varchar, ", 
					"   postcode integer,"  , 
					"   city varchar, ", 
					"   quarter varchar,"  , 
					"   lat double precision, ", 
					"   lon double precision, ", 
					"   precisionHouseNumber varchar, ", 
					"   price double precision, ", 
					"   priceCurrency varchar, ", 
					"   priceMarketingType varchar, ", 
					"   pricePriceIntervalType varchar, ", 
					"   calculatedPriceValue double precision, ", 
					"   calculatedPriceCurrency varchar, ", 
					"   calculatedPriceMarketingType varchar, ", 
					"   calculatedPricePriceIntervalType varchar,   ", 
					"   calculatedPriceRentScope varchar, ", 
					"   area double precision, ", 
					"   rooms double precision, ", 
					"   kitchen boolean,"  , 
					"   balcony boolean, ", 
					"   garden boolean,"  , 
					"   courtage boolean,   ", 
					"   imageUrlSmall varchar,"  , 
					"   imageScaleSmall varchar, ", 
					"   imageUrlLarge varchar,"  , 
					"   imageScaleLarge varchar ", 
					");"
		    }, " "));
			statement.execute("DELETE FROM apartment;");
			statement.close();
		} 
		catch (SQLException e) {
		
			e.printStackTrace();
		}
	}
	
	/**
	 * @param apartments
	 * @throws SQLException
	 */
	public void writeInDB(List<Apartment> apartments) throws SQLException {
		
		Statement statement = backendConnection.createStatement();
		
		StringBuilder insert = new StringBuilder();
		
		int noCoord = 0;
		
		for ( Apartment apartment : apartments )  {
			
			if ( apartment.getLat() != 0) {
				
				insert.append("INSERT INTO apartment (");
				insert.append("	 id , ");
				insert.append("	 title , ");
				insert.append("	 modification , ");
				insert.append("	 creation , ");
				insert.append("	 publishDate , ");
				insert.append("	 realEstateId , ");
				insert.append("	 street ,");
				insert.append("	 houseNumber ,"); 
				insert.append("	 postcode ,");
				insert.append("	 city ,");
				insert.append("	 quarter ,"); 
				insert.append("	 lat ,"); 
				insert.append("	 lon ,");
				insert.append("	 precisionHouseNumber ,"); 
				insert.append("	 price ,"); 
				insert.append("	 priceCurrency ,");
				insert.append("	 priceMarketingType ,"); 
				insert.append("	 pricePriceIntervalType ,"); 
				insert.append("	 calculatedPriceValue ,"); 
				insert.append("	 calculatedPriceCurrency ,"); 
				insert.append("	 calculatedPriceMarketingType ,"); 
				insert.append("	 calculatedPricePriceIntervalType ,	"); 
				insert.append("	 calculatedPriceRentScope ," );
				insert.append("	 area ,"); 
				insert.append("	 rooms ,"); 
				insert.append("	 kitchen ,"); 
				insert.append("	 balcony ,"); 
				insert.append("	 garden ,");
				insert.append("	 courtage ,	"); 
				insert.append("	 imageUrlSmall ,");
				insert.append("	 imageScaleSmall, ");  
				insert.append("	 imageUrlLarge ,");
				insert.append("	 imageScaleLarge ");  
				insert.append(")  VALUES ");
		
				insert.append("(");
				insert.append(apartment.getId());
				insert.append(",'" );
				insert.append(apartment.getTitle());
				insert.append("','" );
				insert.append(apartment.getModification());
				insert.append("','" );
				insert.append(apartment.getCreation());
				insert.append("','" );
				insert.append(apartment.getPublishDate());
				insert.append("'," );
				insert.append(apartment.getRealEstateId());
				insert.append(",'" );
				insert.append(apartment.getStreet());
				insert.append("','");
				insert.append(apartment.getHouseNumber());
				insert.append("', " );
				insert.append(apartment.getPostcode());
				insert.append(", '" );
				insert.append(apartment.getCity());
				insert.append("','" );
				insert.append(apartment.getQuarter());
				insert.append("', " );
				insert.append(apartment.getLat());
				insert.append(", " );
				insert.append(apartment.getLon());
				insert.append(",'" );
				insert.append(apartment.getPrecisionHouseNumber());
				insert.append("', " );
				insert.append(apartment.getPrice());
				insert.append(",'" );
				insert.append(apartment.getPriceCurrency());
				insert.append("','" );
				insert.append(apartment.getPriceMarketingType());
				insert.append(	"','" );
				insert.append(apartment.getPricePriceIntervalType());
				insert.append("','" );
				insert.append(apartment.getCalculatedPriceValue());
				insert.append("','" );
				insert.append(apartment.getCalculatedPriceCurrency());
				insert.append("','" );
				insert.append(apartment.getCalculatedPriceMarketingType());
				insert.append("','" );
				insert.append(apartment.getPricePriceIntervalType());
				insert.append("','" );
				insert.append(apartment.getCalculatedPriceRentScope());
				insert.append("','" );
				insert.append(apartment.getArea());
				insert.append("','" );
				insert.append(apartment.getRooms());
				insert.append("','" );
				insert.append(apartment.isKitchen());
				insert.append("','" );
				insert.append(apartment.isBalcony());
				insert.append("','" );
				insert.append(apartment.isGarden());
				insert.append("','" );
				insert.append(apartment.isCourtage());
				insert.append("','" );
				insert.append(apartment.getImageUrlSmall());
				insert.append("','" );
				insert.append(apartment.getImageScaleSmall());
				insert.append("','" );
				insert.append(apartment.getImageUrlLarge());
				insert.append("','" );
				insert.append(apartment.getImageScaleLarge());
				insert.append("'); ");
			}
			
			else noCoord++;
		}
		statement.execute(insert.toString());
		statement.close();
		
		LOGGER.info(String.format("No coordinates found for %s apartments, skipped.", noCoord));
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Apartment findById(Integer id) {
		
		Session session = HibernateUtil.getInstance().getSessionFactory().openSession();
		Apartment poi = (Apartment) session.byId(Apartment.class).load(id);
		session.close();
		
		return poi;
	}

	public List<Apartment> getApartments(List<Integer> ids) {
		
		Session session 	 = HibernateUtil.getInstance().getSessionFactory().openSession();
		Criteria query		 = session.createCriteria(Apartment.class).add(Restrictions.in("id", ids));
		
		@SuppressWarnings("unchecked")
		List<Apartment> list = query.list();
		session.close();
		
		return list;
	}
}
