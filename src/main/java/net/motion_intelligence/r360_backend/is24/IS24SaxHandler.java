package net.motion_intelligence.r360_backend.is24;

import java.util.List;

import net.motion_intelligence.r360_backend.pojo.Apartment;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class IS24SaxHandler extends DefaultHandler {
	
	boolean realEstateId 			= false;
	boolean distance 				= false;
	boolean street 					= false;
	boolean houseNumber 			= false;
	boolean title 					= false;
	boolean postcode 				= false;
	boolean city 					= false;
	boolean quarter 				= false;
	boolean latitude				= false;
	boolean longitude 			 	= false;
	boolean precisionHouseNumber	= false;
	boolean price 					= false;
	boolean value 					= false;
	boolean currency 				= false;
	boolean marketingType 			= false;
	boolean priceIntervalType 		= false;
	boolean livingSpace 			= false;
	boolean numberOfRooms 			= false;
	boolean builtInKitchen 			= false;
	boolean balcony 				= false;
	boolean garden 					= false;
	boolean courtage 				= false;
	boolean calculatedPrice 		= false;
	boolean rentScope 				= false;

	Apartment apartment = null;

	boolean pageNumber 				= false;
	boolean numberOfPages 			= false;
	boolean paging 					= false;
	
	private List<Apartment> apartments;
	
	/**
	 * @param apartments
	 */
	public IS24SaxHandler(List<Apartment> apartments) {
		
		this.apartments = apartments;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		// System.out.println("Start Element :" + qName);
		if (qName.equalsIgnoreCase("paging")) paging = true;
		if (qName.equalsIgnoreCase("numberOfPages")) numberOfPages = true;
		if (qName.equalsIgnoreCase("pageNumber")) pageNumber = true;
		if (qName.equalsIgnoreCase("realEstateId")) realEstateId = true;
		if (qName.equalsIgnoreCase("distance")) distance = true;
		if (qName.equalsIgnoreCase("title")) title = true;
		if (qName.equalsIgnoreCase("street")) street = true;
		if (qName.equalsIgnoreCase("houseNumber")) houseNumber = true;
		if (qName.equalsIgnoreCase("postcode")) postcode = true;
		if (qName.equalsIgnoreCase("city")) city = true;
		if (qName.equalsIgnoreCase("quarter")) quarter = true;
		if (qName.equalsIgnoreCase("latitude")) latitude = true;
		if (qName.equalsIgnoreCase("longitude")) longitude = true;
		if (qName.equalsIgnoreCase("precisionHouseNumber")) precisionHouseNumber = true;
		if (qName.equalsIgnoreCase("price")) price = true;
		if (qName.equalsIgnoreCase("value")) value = true;
		if (qName.equalsIgnoreCase("currency")) currency = true;
		if (qName.equalsIgnoreCase("marketingType")) marketingType = true;
		if (qName.equalsIgnoreCase("priceIntervalType")) priceIntervalType = true;
		if (qName.equalsIgnoreCase("livingSpace")) livingSpace = true;
		if (qName.equalsIgnoreCase("numberOfRooms")) numberOfRooms = true;
		if (qName.equalsIgnoreCase("builtInKitchen")) builtInKitchen = true;
		if (qName.equalsIgnoreCase("balcony")) balcony = true;
		if (qName.equalsIgnoreCase("garden")) garden = true;
		if (qName.equalsIgnoreCase("hasCourtage")) courtage = true;
		if (qName.equalsIgnoreCase("calculatedPrice")) calculatedPrice = true;
		if (qName.equalsIgnoreCase("rentScope")) rentScope = true;
		
		if (qName.equalsIgnoreCase("resultlistEntry")) {

			apartment = new Apartment();
			this.apartments.add(apartment);

			if (attributes.getLength() > 0) {
				
				for (int i = 0; i < attributes.getLength(); i++) {
					
					if (attributes.getQName(i).equalsIgnoreCase("id")) apartment.setId(Integer.parseInt(attributes.getValue(i)));
					if (attributes.getQName(i).equalsIgnoreCase("modification")) apartment.setModification(attributes.getValue(i));
					if (attributes.getQName(i).equalsIgnoreCase("creation")) apartment.setCreation(attributes.getValue(i));
					if (attributes.getQName(i).equalsIgnoreCase("publishDate")) apartment.setPublishDate(attributes.getValue(i));
				}
			}
		}
		
		if ( qName.equalsIgnoreCase("url") ) {
			
			String scale = null;
			String url = null;
			
			if (attributes.getLength() > 0) {
				
				for (int i = 0; i < attributes.getLength(); i++) {
					
					if (attributes.getQName(i).equalsIgnoreCase("scale")) scale = attributes.getValue(i);
					if (attributes.getQName(i).equalsIgnoreCase("href")) url = attributes.getValue(i);
				}
				
				if (scale.equalsIgnoreCase("SCALE_118x118")) {
					
					apartment.setImageUrlSmall(url);
					apartment.setImageScaleSmall(scale);
				} 
				else if (scale.equalsIgnoreCase("SCALE_210x210")) {
					
					apartment.setImageUrlLarge(url);
					apartment.setImageScaleLarge(scale);
				}
			}
		}

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		
		if (qName.equalsIgnoreCase("paging")) 				paging = false;
		if (qName.equalsIgnoreCase("numberOfPages")) 		numberOfPages = false;
		if (qName.equalsIgnoreCase("pageNumber")) 			pageNumber = false;
		if (qName.equalsIgnoreCase("price")) 				price = false;
		if (qName.equalsIgnoreCase("value")) 				value = false;
		if (qName.equalsIgnoreCase("currency")) 			currency = false;
		if (qName.equalsIgnoreCase("marketingType")) 		marketingType = false;
		if (qName.equalsIgnoreCase("priceIntervalType")) 	priceIntervalType = false;
		if (qName.equalsIgnoreCase("calculatedPrice")) 		calculatedPrice = false;
		if (qName.equalsIgnoreCase("rentScope")) 			rentScope = false;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	public void characters(char ch[], int start, int length)  throws SAXException {

		if (paging && pageNumber) {
			
			Immobilienscout24ImportTask.pageNumberGlobal = Integer.parseInt(new String(ch, start, length));
			pageNumber = false;
		}
		if (paging && numberOfPages) {
			
			Immobilienscout24ImportTask.numberOfPagesGlobal = Integer.parseInt(new String(ch, start, length));
			numberOfPages = false;
		}

		if (realEstateId) {
			apartment.setRealEstateId(Integer.parseInt(new String(ch, start, length)));
			realEstateId = false;
		}

		if (distance) distance = false;

		if (street) {
			
			apartment.setStreet(new String(ch, start, length).replace("'", "\'\'").replace("\"", "\"\""));
			street = false;
		}
		
		if (title) {
			
			apartment.setTitle(new String(ch, start, length).replace("'", "\'\'").replace("\"", "\"\""));
			title = false;
		}

		if (houseNumber) {
			
			apartment.setHouseNumber(new String(ch, start, length));
			houseNumber = false;
		}
		if (postcode) {
			
			apartment.setPostcode(Integer.parseInt(new String(ch, start, length).trim()));
			postcode = false;
		}
		if (city) {
			
			apartment.setCity(new String(ch, start, length));
			city = false;
		}
		
		if (quarter) {
			
			apartment.setQuarter(new String(ch, start, length));
			quarter = false;
		}
		
		if (latitude) {
			
			apartment.setLat(Double.parseDouble(new String(ch, start, length)));
			latitude = false;
		}
		
		if (longitude) {
			
			apartment.setLon(Double.parseDouble(new String(ch, start, length)));
			longitude = false;
		}
		
		if (precisionHouseNumber) {
			apartment.setPrecisionHouseNumber(new String(ch, start, length));
			precisionHouseNumber = false;
		}
		
		if (value && price) apartment.setPrice(Double.parseDouble(new String(ch, start, length)));
		if (currency && price) apartment.setPriceCurrency(new String(ch, start, length));
		
		if (marketingType && price) apartment.setPriceMarketingType(new String(ch, start, length));
		if (priceIntervalType && price) apartment.setPricePriceIntervalType(new String(ch, start, length));

		if (livingSpace) {
			
			apartment.setArea(Double.parseDouble(new String(ch, start, length)));
			livingSpace = false;
		}
		
		if (numberOfRooms) {
		
			apartment.setRooms(Double.parseDouble(new String(ch, start, length)));
			numberOfRooms = false;
		}
		
		if (builtInKitchen) {
			
			apartment.setKitchen(Boolean.parseBoolean(new String(ch, start, length)));
			builtInKitchen = false;
		}
		
		if (balcony) {
			
			apartment.setBalcony(Boolean.parseBoolean(new String(ch, start, length)));
			balcony = false;
		}
		
		if (garden) {
			
			apartment.setGarden(Boolean.parseBoolean(new String(ch, start, length)));
			garden = false;
		}
		
		if (courtage) {
			
			apartment.setCourtage(new String(ch, start, length).equals("YES") ? true : false);
			courtage = false;
		}
		if (value && calculatedPrice) apartment.setCalculatedPriceValue(Double.parseDouble(new String(ch, start, length)));
		if (currency && calculatedPrice) apartment.setCalculatedPriceCurrency(new String(ch, start, length));
		if (marketingType && calculatedPrice) apartment.setCalculatedPriceMarketingType(new String(ch, start, length));
		if (priceIntervalType && calculatedPrice) apartment.setCalculatedPricePriceIntervalType(new String(ch, start, length));
		if (rentScope && calculatedPrice) apartment.setCalculatedPriceRentScope(new String(ch, start, length));
	}
}
