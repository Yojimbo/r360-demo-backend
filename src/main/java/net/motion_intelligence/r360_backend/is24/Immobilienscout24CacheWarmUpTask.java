package net.motion_intelligence.r360_backend.is24;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.message.GZipEncoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.motion_intelligence.r360_backend.R360BackendConfig;
import net.motion_intelligence.r360_backend.pojo.Apartment;

public class Immobilienscout24CacheWarmUpTask {
	
	private static ApartmentDao dao 		= new ApartmentDao();
	private static final Logger LOGGER 		= LoggerFactory.getLogger(Immobilienscout24CacheWarmUpTask.class);
	private static R360BackendConfig CONFIG = R360BackendConfig.loadConfig();
	
	public static void warmCache() {
		
		try {
			
			// define the configuration
			JSONObject payLoad = new JSONObject();
			payLoad.put("maxRoutingTime", "7200");
			payLoad.put("pathSerializer", "compact");
			payLoad.put("sources", new JSONArray());
			payLoad.put("targets", new JSONArray());
			
			// create a single source
			JSONObject source = new JSONObject();
			source.put("id", "52.517037;13.38886");
			source.put("lat","52.517037");
			source.put("lon","13.38886");
			source.put("tm" , new JSONObject());
			source.getJSONObject("tm").put("transit", new JSONObject());
			
			// make transit routing for today at noon
			JSONObject frame = new JSONObject();
			frame.put("time", "43200");
			frame.put("date", new SimpleDateFormat("yyyyMMdd").format(new Date()));
			source.getJSONObject("tm").getJSONObject("transit").put("frame", frame);
			
			// add the source
			payLoad.getJSONArray("sources").put(source);
			
			// add all apartments as targets
			for ( Apartment apartment : dao.getAllApartments() ) {
				
				JSONObject target = new JSONObject();
				target.put("id", apartment.getLat() + ";" + apartment.getLon());
				target.put("lat",apartment.getLat());
				target.put("lon",apartment.getLon());
				payLoad.getJSONArray("targets").put(target);
			}
			
	        Client client = ClientBuilder.newClient();
	        WebTarget target = client.target(CONFIG.getStringSetting("general", "r360-api-url")).register(GZipEncoder.class).queryParam("key", "iWJUcDfMWTzVDL69EWCG");
	        
	        Response response = target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(payLoad.toString(), MediaType.APPLICATION_JSON), Response.class);
	        LOGGER.info(String.format("Cache warmup complete! %s", response.readEntity(String.class)));
	        client.close();
		} 
		catch (JSONException e) {
			
			e.printStackTrace();
			LOGGER.error(String.format("Could not warm up cache.", e));
		}
		
	}
}
