/**
 * 
 */
package net.motion_intelligence.r360_backend.is24;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gerb
 *
 */
public class IS24ImportJob implements Job {

	private static final Logger LOGGER = LoggerFactory.getLogger(IS24ImportJob.class);
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		LOGGER.info("Starting to import is24 apartments");
		Immobilienscout24ImportTask.importApartments();
		Immobilienscout24CacheWarmUpTask.warmCache();
	}
}
