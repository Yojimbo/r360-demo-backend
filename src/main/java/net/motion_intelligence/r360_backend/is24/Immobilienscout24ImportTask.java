package net.motion_intelligence.r360_backend.is24;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import net.motion_intelligence.r360_backend.pojo.Apartment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class Immobilienscout24ImportTask {

	public static Integer pageNumberGlobal		= 1;
	public static Integer numberOfPagesGlobal	= 0;
	private static List<Apartment> apartments	= new ArrayList<>();
	
	private static final Logger LOGGER 			= LoggerFactory.getLogger(Immobilienscout24ImportTask.class);
	
	public static void main(String[] args) {
		
		Immobilienscout24ImportTask.importApartments();
	}

	public static void importApartments() {
		
		long start = System.currentTimeMillis();
		apartments	= new ArrayList<>();
		
		do {
			
			// crawl berlin stuff
			crawlImmoScoutXml("1276003001");
			pageNumberGlobal++;
		} 
		while ( pageNumberGlobal <= numberOfPagesGlobal );
		
		// reset the page counters
		pageNumberGlobal = 1;
		numberOfPagesGlobal = 0;
		
		do {

			// Brandenburg 
			crawlImmoScoutXml("1276004");
			pageNumberGlobal++;
		} 
		while ( pageNumberGlobal <= numberOfPagesGlobal );
		
		LOGGER.info(String.format("It took %ss to download %s apartments!",((System.currentTimeMillis() - start) / 1000 ), apartments.size()));
		start = System.currentTimeMillis();
		
		try {
			
			ApartmentDao dao = new ApartmentDao();
			// now we have all the apartments, lets write them to the database
			dao.createNewTable();
			dao.writeInDB(apartments);
			
			LOGGER.info(String.format("It took %ss to write %s apartments to the database!",((System.currentTimeMillis() - start) / 1000 ), apartments.size()));
		} 
		catch (SQLException e) {
			
			LOGGER.error("Could not write apartments into database", e);
			e.printStackTrace();
		}
	}

	private static void crawlImmoScoutXml(String geocode) {

		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			
			String query = "http://rest.immobilienscout24.de/restapi/api/search/v1.0/search/region?realestatetype=apartmentrent&geocodes="+geocode+"&pageNumber=" + pageNumberGlobal + "&pageSize=200";
			LOGGER.info("Running query: " + query);
			
			saxParser.parse(new URL(query).openStream(), new IS24SaxHandler(apartments));
		} 
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
