/**
 * 
 */
package net.motion_intelligence.r360_backend.service.json;

import java.util.List;

import net.motion_intelligence.r360_backend.Constants;
import net.motion_intelligence.r360_backend.pojo.Apartment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author gerb
 *
 */
public class MinimalApartmentSerializer {
	
	public static String serialize(List<Apartment> apartments) {
		
		JSONArray pois = new JSONArray();
		for ( Apartment apartment : apartments ) {
			
			try {
					
				JSONObject jsonPoi = new JSONObject();
				jsonPoi.put(Constants.APARTMENT_ID, apartment.getId());
				jsonPoi.put(Constants.APARTMENT_TITLE, apartment.getTitle());
				jsonPoi.put(Constants.APARTMENT_PRICE, apartment.getPrice());
				jsonPoi.put(Constants.APARTMENT_ROOMS, apartment.getRooms());
				jsonPoi.put(Constants.APARTMENT_AREA, apartment.getArea());
				jsonPoi.put(Constants.APARTMENT_IMAGE, !apartment.getImageUrlLarge().isEmpty()? apartment.getImageUrlLarge() : apartment.getImageUrlSmall());
				jsonPoi.put(Constants.APARTMENT_LAT, Math.floor(apartment.getLat() * 1e5) / 1e5);
				jsonPoi.put(Constants.APARTMENT_LON, Math.floor(apartment.getLon() * 1e5) / 1e5);
				pois.put(jsonPoi);
			}
			catch (JSONException e) {
				
				e.printStackTrace();
			}
		}
		
		return pois.toString();
	}
}
