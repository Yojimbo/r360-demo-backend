package net.motion_intelligence.r360_backend.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.JSONP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.motion_intelligence.r360_backend.is24.ApartmentDao;
import net.motion_intelligence.r360_backend.pojo.Apartment;
import net.motion_intelligence.r360_backend.service.json.MinimalApartmentSerializer;

@Path("apartments")
public class ApartmentService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApartmentService.class);
	ApartmentDao apartmentDao = new ApartmentDao();
	
	@GET
	@Path("all")
	@JSONP(queryParam="callback")
	@Produces({"application/x-javascript"})
	public String getAll(
			@DefaultValue("300") @QueryParam("minPrice") 	double minPrice,
			@DefaultValue("800") @QueryParam("maxPrice") 	double maxPrice,
			@DefaultValue("2") @QueryParam("minRooms") 		double minRooms,
			@DefaultValue("3") @QueryParam("maxRooms") 		double maxRooms,
			@DefaultValue("50") @QueryParam("minAra") 		double minAra,
			@DefaultValue("80") @QueryParam("maxArea") 		double maxArea,
			@DefaultValue("false") @QueryParam("kitchen") 	boolean kitchen,
			@DefaultValue("false") @QueryParam("garden") 	boolean garden,
			@DefaultValue("false") @QueryParam("courtage") 	boolean courtageFree,
			@DefaultValue("false") @QueryParam("balcony") 	boolean balcony,
			@QueryParam("callback") String callback) {
		
		List<Apartment> apartments = apartmentDao.getApartments(minPrice, maxPrice, minAra, maxArea, minRooms, maxRooms, kitchen, garden, courtageFree, balcony);
		
		LOGGER.info(String.format("Found %s apartments for query: minPrice: %s, maxPrice: %s, minAra: %s, maxArea: %s, minRooms: %s, maxRooms: %s, kitchen: %s, garden: %s, courtageFree: %s, balcony: %s",
				apartments.size(), minPrice, maxPrice, minAra, maxArea, minRooms, maxRooms, kitchen, garden, courtageFree, balcony));
		
		return MinimalApartmentSerializer.serialize(apartments);
	}
	
	@GET
	@Path("list")
	@JSONP(queryParam="callback")
	@Produces({"application/x-javascript"})
	public List<Apartment> getById(@QueryParam("id") List<Integer> ids, @QueryParam("callback") String callback) {
		
		List<Apartment> apartments = apartmentDao.getApartments(ids);
		LOGGER.info(String.format("Returned %s apartments by ids", apartments.size()));
		
		return apartments;
	}
	
	@GET 
	@Path("{id}")
	@JSONP(queryParam="callback")
	@Produces({"application/x-javascript"})
	public Apartment findById(@PathParam("id") Integer id, @QueryParam("callback") String callback) {
		
		Apartment apartment = apartmentDao.findById(id);
		LOGGER.info(String.format("Get details for poi: %s", id));
	    return apartment;
	}
}