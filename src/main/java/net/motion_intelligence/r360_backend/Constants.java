package net.motion_intelligence.r360_backend;

public class Constants {

	public static final String APARTMENT_ID		= "id";
	public static final String APARTMENT_TITLE	= "title";
	public static final String APARTMENT_LAT 	= "lat";
	public static final String APARTMENT_LON 	= "lon";
	public static final String APARTMENT_PRICE = "price";
	public static final String APARTMENT_ROOMS = "rooms";
	public static final String APARTMENT_AREA = "area";
	public static final String APARTMENT_IMAGE = "img";

}
