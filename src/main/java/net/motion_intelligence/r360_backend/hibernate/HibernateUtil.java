/**
 * 
 */
package net.motion_intelligence.r360_backend.hibernate;

import net.motion_intelligence.r360_backend.pojo.Apartment;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * @author Daniel Gerber <daniel.gerber@deinestadtsuchtdich.de>
 *
 */
public class HibernateUtil {
	
	private static HibernateUtil INSTANCE = null;
	
	private SessionFactory sessionFactory;
	private ServiceRegistry serviceRegistry;
	
	private HibernateUtil(){
		
		Configuration config = new Configuration().configure()
				.addAnnotatedClass(Apartment.class);
		
		serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
		sessionFactory  = config.buildSessionFactory(serviceRegistry);
	};
	
	public static HibernateUtil getInstance() {
		
		if ( HibernateUtil.INSTANCE == null ) {
			
			INSTANCE = new HibernateUtil();
		}
		
		return HibernateUtil.INSTANCE;
	}

	/**
	 * @return the sessionFactory
	 */
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	/**
	 * 
	 */
	public void close(){
		
		this.sessionFactory.close();
	}
}
