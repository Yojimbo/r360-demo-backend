package net.motion_intelligence.r360_backend.pojo;

import java.beans.Transient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "apartment")
@XmlRootElement
public class Apartment {
	
	@Id @GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name = "imageUrlLarge")
	private String imageUrlLarge;
	
	@Column(name = "imageScaleLarge")
	private String imageScaleLarge;
	
	@Column(name = "street")
	private String street;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "houseNumber")
	private String houseNumber;
	
	@Column(name = "postcode")
	private int postcode;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "quarter")
	private String quarter;
	
	@Column(name = "lat")
	private double lat;
	
	@Column(name = "lon")
	private double lon;
	
	@Column(name = "price")
	private double price;
	
	@Column(name = "area")
	private double area;
	
	@Column(name = "rooms")
	private double rooms;
	
	@Column(name = "kitchen")
	private boolean kitchen;
	
	@Column(name = "balcony")
	private boolean balcony;
	
	@Column(name = "garden")
	private boolean garden;
	
	@Column(name = "courtage")
	private boolean courtage;

	@Column(name = "calculatedPriceValue")	
	private double calculatedPriceValue;
	
	private String calculatedPriceCurrency;
	private String calculatedPriceMarketingType;
	private String calculatedPricePriceIntervalType;
	private String calculatedPriceRentScope;
	private int realEstateId;
	private String precisionHouseNumber;
	private String priceCurrency;
	private String priceMarketingType;
	private String pricePriceIntervalType;
	private String modification;
	private String creation;
	private String publishDate;
	private String imageUrlSmall;
	private String imageScaleSmall;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the imageUrlLarge
	 */
	public String getImageUrlLarge() {
		return imageUrlLarge;
	}
	/**
	 * @param imageUrlLarge the imageUrlLarge to set
	 */
	public void setImageUrlLarge(String imageUrlLarge) {
		this.imageUrlLarge = imageUrlLarge;
	}
	/**
	 * @return the imageScaleLarge
	 */
	public String getImageScaleLarge() {
		return imageScaleLarge;
	}
	/**
	 * @param imageScaleLarge the imageScaleLarge to set
	 */
	public void setImageScaleLarge(String imageScaleLarge) {
		this.imageScaleLarge = imageScaleLarge;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the houseNumber
	 */
	public String getHouseNumber() {
		return houseNumber;
	}
	/**
	 * @param houseNumber the houseNumber to set
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	/**
	 * @return the postcode
	 */
	public int getPostcode() {
		return postcode;
	}
	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the quarter
	 */
	public String getQuarter() {
		return quarter;
	}
	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	/**
	 * @return the latitude
	 */
	public double getLat() {
		return lat;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLat(double latitude) {
		this.lat = latitude;
	}
	/**
	 * @return the longitude
	 */
	public double getLon() {
		return lon;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLon(double longitude) {
		this.lon = longitude;
	}
	/**
	 * @return the price_value
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price_value the price_value to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the area
	 */
	public double getArea() {
		return area;
	}
	/**
	 * @param area the area to set
	 */
	public void setArea(double area) {
		this.area = area;
	}
	/**
	 * @return the numberOfRooms
	 */
	public double getRooms() {
		return rooms;
	}
	/**
	 * @param rooms the rooms to set
	 */
	public void setRooms(double rooms) {
		this.rooms = rooms;
	}
	/**
	 * @return the builtInKitchen
	 */
	public boolean isKitchen() {
		return kitchen;
	}
	/**
	 * @param builtInKitchen the builtInKitchen to set
	 */
	public void setKitchen(boolean kitchen) {
		this.kitchen = kitchen;
	}
	/**
	 * @return the balcony
	 */
	public boolean isBalcony() {
		return balcony;
	}
	/**
	 * @param balcony the balcony to set
	 */
	public void setBalcony(boolean balcony) {
		this.balcony = balcony;
	}
	/**
	 * @return the garden
	 */
	public boolean isGarden() {
		return garden;
	}
	/**
	 * @param garden the garden to set
	 */
	public void setGarden(boolean garden) {
		this.garden = garden;
	}
	/**
	 * @return the hasCourtage
	 */
	public boolean isCourtage() {
		return courtage;
	}
	/**
	 * @param hasCourtage the hasCourtage to set
	 */
	public void setCourtage(boolean courtage) {
		this.courtage = courtage;
	}
	/**
	 * @return the realEstateId
	 */
	public int getRealEstateId() {
		return realEstateId;
	}
	/**
	 * @param realEstateId the realEstateId to set
	 */
	public void setRealEstateId(int realEstateId) {
		this.realEstateId = realEstateId;
	}
	/**
	 * @return the precisionHouseNumber
	 */
	public String getPrecisionHouseNumber() {
		return precisionHouseNumber;
	}
	/**
	 * @param precisionHouseNumber the precisionHouseNumber to set
	 */
	public void setPrecisionHouseNumber(String precisionHouseNumber) {
		this.precisionHouseNumber = precisionHouseNumber;
	}
	/**
	 * @return the priceCurrency
	 */
	public String getPriceCurrency() {
		return priceCurrency;
	}
	/**
	 * @param priceCurrency the priceCurrency to set
	 */
	public void setPriceCurrency(String priceCurrency) {
		this.priceCurrency = priceCurrency;
	}
	/**
	 * @return the priceMarketingType
	 */
	public String getPriceMarketingType() {
		return priceMarketingType;
	}
	/**
	 * @param priceMarketingType the priceMarketingType to set
	 */
	public void setPriceMarketingType(String priceMarketingType) {
		this.priceMarketingType = priceMarketingType;
	}
	/**
	 * @return the pricePriceIntervalType
	 */
	public String getPricePriceIntervalType() {
		return pricePriceIntervalType;
	}
	/**
	 * @param pricePriceIntervalType the pricePriceIntervalType to set
	 */
	public void setPricePriceIntervalType(String pricePriceIntervalType) {
		this.pricePriceIntervalType = pricePriceIntervalType;
	}
	/**
	 * @return the modification
	 */
	public String getModification() {
		return modification;
	}
	/**
	 * @param modification the modification to set
	 */
	public void setModification(String modification) {
		this.modification = modification;
	}
	/**
	 * @return the creation
	 */
	public String getCreation() {
		return creation;
	}
	/**
	 * @param creation the creation to set
	 */
	public void setCreation(String creation) {
		this.creation = creation;
	}
	/**
	 * @return the publishDate
	 */
	public String getPublishDate() {
		return publishDate;
	}
	/**
	 * @param publishDate the publishDate to set
	 */
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	/**
	 * @return the imageUrlSmall
	 */
	public String getImageUrlSmall() {
		return imageUrlSmall;
	}
	/**
	 * @param imageUrlSmall the imageUrlSmall to set
	 */
	public void setImageUrlSmall(String imageUrlSmall) {
		this.imageUrlSmall = imageUrlSmall;
	}
	/**
	 * @return the imageScaleSmall
	 */
	public String getImageScaleSmall() {
		return imageScaleSmall;
	}
	/**
	 * @param imageScaleSmall the imageScaleSmall to set
	 */
	public void setImageScaleSmall(String imageScaleSmall) {
		this.imageScaleSmall = imageScaleSmall;
	}
	/**
	 * @return the calculatedPriceValue
	 */
	public double getCalculatedPriceValue() {
		return calculatedPriceValue;
	}
	/**
	 * @param calculatedPriceValue the calculatedPriceValue to set
	 */
	public void setCalculatedPriceValue(double calculatedPriceValue) {
		this.calculatedPriceValue = calculatedPriceValue;
	}
	/**
	 * @return the calculatedPriceCurrency
	 */
	public String getCalculatedPriceCurrency() {
		return calculatedPriceCurrency;
	}
	/**
	 * @param calculatedPriceCurrency the calculatedPriceCurrency to set
	 */
	public void setCalculatedPriceCurrency(String calculatedPriceCurrency) {
		this.calculatedPriceCurrency = calculatedPriceCurrency;
	}
	/**
	 * @return the calculatedPriceMarketingType
	 */
	public String getCalculatedPriceMarketingType() {
		return calculatedPriceMarketingType;
	}
	/**
	 * @param calculatedPriceMarketingType the calculatedPriceMarketingType to set
	 */
	public void setCalculatedPriceMarketingType(String calculatedPriceMarketingType) {
		this.calculatedPriceMarketingType = calculatedPriceMarketingType;
	}
	/**
	 * @return the calculatedPricePriceIntervalType
	 */
	public String getCalculatedPricePriceIntervalType() {
		return calculatedPricePriceIntervalType;
	}
	/**
	 * @param calculatedPricePriceIntervalType the calculatedPricePriceIntervalType to set
	 */
	public void setCalculatedPricePriceIntervalType(
			String calculatedPricePriceIntervalType) {
		this.calculatedPricePriceIntervalType = calculatedPricePriceIntervalType;
	}
	/**
	 * @return the calculatedPriceRentScope
	 */
	public String getCalculatedPriceRentScope() {
		return calculatedPriceRentScope;
	}
	/**
	 * @param calculatedPriceRentScope the calculatedPriceRentScope to set
	 */
	public void setCalculatedPriceRentScope(String calculatedPriceRentScope) {
		this.calculatedPriceRentScope = calculatedPriceRentScope;
	}
}
