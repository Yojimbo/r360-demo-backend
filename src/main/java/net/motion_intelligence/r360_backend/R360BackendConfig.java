/**
 * 
 */
package net.motion_intelligence.r360_backend;

import java.io.IOException;

import org.ini4j.Ini;

/**
 * @author Daniel Gerber
 * 
 */
public class R360BackendConfig {

	private Ini naturtripConfig;

	private R360BackendConfig(Ini config) {

		this.naturtripConfig = config;
	}

	/**
	 * returns boolean values from the config file
	 * 
	 * @param section
	 * @param key
	 * @return
	 */
	public boolean getBooleanSetting(String section, String key) {

		return Boolean.valueOf(naturtripConfig.get(section, key));
	}

	/**
	 * returns string values from bsd config
	 * 
	 * @param section
	 * @param key
	 * @return
	 */
	public String getStringSetting(String section, String key) {

		return naturtripConfig.get(section, key);
	}

	/**
	 * this should overwrite a config setting, TODO make sure that it does
	 * 
	 * @param string
	 * @param string2
	 */
	public void setStringSetting(String section, String key, String value) {

		this.naturtripConfig.add(section, key, value);
	}

	/**
	 * returns integer values for bsd setting
	 * 
	 * @param section
	 * @param key
	 * @return
	 */
	public Integer getIntegerSetting(String section, String key) {

		return Integer.valueOf(this.naturtripConfig.get(section, key));
	}

	/**
	 * returns double values from the config
	 * 
	 * @param section
	 * @param key
	 * @return
	 */
	public Double getDoubleSetting(String section, String key) {

		return Double.valueOf(this.naturtripConfig.get(section, key));
	}
	
	/**
	 * 
	 * @param section
	 * @param key
	 * @return
	 */
	public int getIntSetting(String section, String key) {
		
		return Integer.valueOf(this.naturtripConfig.get(section, key));
	}

	/**
	 * Load the config file from the classpath.
	 * @return the naturtrip config object
	 */
	public static R360BackendConfig loadConfig() {

		try {

			return new R360BackendConfig(new Ini(R360BackendConfig.class.getResourceAsStream("/r360.ini")));
		} 
		catch (IOException e) {

			throw new RuntimeException("Could not load config file: " + e);
		}
	}
}
