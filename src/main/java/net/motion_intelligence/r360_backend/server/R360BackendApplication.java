/**
 * 
 */
package net.motion_intelligence.r360_backend.server;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Daniel Gerber <daniel.gerber@icloud.com>
 *
 */
@ApplicationPath("")
public class R360BackendApplication extends ResourceConfig {
	
	public R360BackendApplication() {
        packages("net.motion_intelligence.r360_backend.service;net.motion_intelligence.r360_backend.server");
    }
}