/**
 * 
 */
package net.motion_intelligence.r360_backend.server;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import net.motion_intelligence.r360_backend.R360BackendConfig;
import net.motion_intelligence.r360_backend.hibernate.HibernateUtil;
import net.motion_intelligence.r360_backend.is24.IS24ImportJob;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Daniel Gerber <daniel.gerber@icloud.com>
 * @author Henning Hollburg
 */
@WebListener
public class ServletContextInitalizer implements ServletContextListener {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServletContextInitalizer.class);
	private R360BackendConfig CONFIG = R360BackendConfig.loadConfig();
    private Scheduler scheduler;

	/**
	 * This method is called only one time at service/server startup.
	 * The main purpose is to start the scheduler with runs the import 
	 * every night at 2 pm.
	 * 
	 * @param ServletContextEvent
	 */
	@Override 
	public void contextInitialized(ServletContextEvent contextEvent) {
		
		LOGGER.info("Scheduling IS24 import job!");
		
		JobDetail job = JobBuilder.newJob(IS24ImportJob.class).withIdentity(IS24ImportJob.class.getSimpleName()).build();
        
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(IS24ImportJob.class.getSimpleName() + "TRIGGER")
                .withSchedule(CronScheduleBuilder.cronSchedule(CONFIG.getStringSetting("general", "cronjob"))).build();
        
		try {

			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			
			// if we want to start the import on server startup
			if ( CONFIG.getBooleanSetting("general", "instantImport") ) {
	        	
				scheduler.scheduleJob(
						JobBuilder.newJob(IS24ImportJob.class).withIdentity(IS24ImportJob.class.getSimpleName() + "JOB_NOW").build(),
						TriggerBuilder.newTrigger().withIdentity(IS24ImportJob.class.getSimpleName() + "TRIGGER_NOW").startNow().build());
	        }
			
	        scheduler.scheduleJob(job, trigger);
		}
		catch (SchedulerException e) {
			
			e.printStackTrace();
			LOGGER.error("Import was not successful!", e);
		}
	}
	
	/**
	 * 
	 */
	@Override 
	public void contextDestroyed(ServletContextEvent contextEvent) {
		
		boolean isShutdown = false;
		
		try {
			
			scheduler.shutdown(false);
			isShutdown = scheduler.isShutdown();
			HibernateUtil.getInstance().close();
		}
		catch (SchedulerException e) {

			e.printStackTrace();
			LOGGER.error("Shutdown of scheduler was not successful!", e);
		}
		
		LOGGER.info("Servlet context destroyed.. Shutting down finish: "+ isShutdown);
	}
}